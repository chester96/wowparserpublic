## WoWParser is a Tool to Parse World of Warcraft files (DBC ADB DB2 WDB) to a various file formats.

[Here](https://bitbucket.org/glkrlos/wowparserpublic) you can get the latest public release of this program.

## WoWParser 3.0 BETA (Not yet released)
    Current changes:
    * Now are two executables one for linux and the other for windows.
    * No longer compiled under Windows.
    * Configuration file changed to XML format.
    * Now uses Version Control (Git).
    * Revision is used instead of Build.
    * Compiled sources now include HASH and DATE from repository to know what Revision is.
    * No longer has two windows executables, now is all in one.
    * Modules to read CSV, DBC, DB2, ADB files are totally redone.
    * Reading CSV files now shows correct messages if expected data is wrong.
    * Predict field types now gets unsigned int fields.

    Still in Development:
    * Module to predict field types in WDB files. (really hard to do that but i'm in progress)
    * Module to read WDB files totally redone.
    * Support to verify converted files.
    * Support to parse data to ADB, DB2 and SQL file format.
    * Support to select name and order of columns in SQL or CSV output format.

## WoWParser 2.0:
    Initial Build:
    * Version 2.0
    Build 12:
    * Improved optimization at read binary files.
    Build 73:
    * Corrected many problemas at parse CSV files.
    * Added many error messages to known the cause of problem at parsing file.
    * Added support to read DB2 files (after patched mode).
    * Finally predict field types working very good with float and string fields.
    * Removed support to read files via arguments in command line.
    * Implemented support to find and read files using recursive mode in the program directory.
    * Implemented support to read files with specific format (string, int, float and byte fields).
    * Added configuration file to read files and format.
    * Implemented in configuration find files using *.dbc for example.
    * Implemented creation of DBC files using recursive mode.
    Build 81:
    *Fixed a bug where Predict System can't open some files because an incorrect error message of byte packed.
    *Improved Predict String Fields from ADB, DB2 and DBC files (Report any bugs if any with this improvement).
    *Improved Parse Strings Fields from CSV files, now display proper error messages in unexpected end of string or missing " at the end of string field.
    Build 85:
    * Fixed a crash if the only one field is set (string field) at parse CSV files.
    * Fixed a problem if the first field is a string, the rest of the fields can cause an incorrect error messages.
    Build 98 (Final 2.0 Version):
    * Improved code optimization to read in a better and safe way binary files.
    * Implemented support to read WDB files (Only in configuration file with the proper format).
    * Improved format in configuration file to read in one special way only itemcache.wdb to parse it correctly.

    Known Bugs in version 2.0:
    * Reading values from CSV file for integer, float, and byte fields still no error message if you put an alphabetical character, normally in conversion to numeric value is cero '0', so beware.

## WoWParser 1.0:
    * Implement support to read ADB, DBC files.
    * Added Basic Support to predict integer, string and float field types (Predict float and string fields still not working good and byte fields not supported yet).
    * Added support to read files via arguments in command line.
     *Implemented Read CSV files.